package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private Stack<Double> stack = new Stack<>();
    String rpn;
    boolean hasDot = false;
    String returnResult;

    public Calculator() {
        stack = new Stack<>();
        rpn = "";
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        //to RPN
        if(statement==null || statement.length()==0) return null;
        if(!statement.matches("^[(]?[-]?([0-9]([//.][0-9]+)?)+([()]*[*+-//]?[()]*([0-9]([/.][0-9]+)?))*[()]*")|| statement.contains(",")) return null;

        ConvertoToRPN convertor = new ConvertoToRPN();
        rpn = convertor.convert(statement);

        if(statement.contains(".")) hasDot =true;


        double tmpNum = 0;
        //if incorrect
        if(rpn.isEmpty()) return null;
        String[] expressionStr = rpn.split(" ");
        for (String str : expressionStr) {
            if(!str.equals("+") && !str.equals("-") &&
                    !str.equals("*") && !str.equals("/")){
                try{
                    tmpNum = Double.valueOf(str);
                }catch(NumberFormatException ex){
                    //if its not a digit return null
                    return null;
                }
                stack.push(tmpNum);
            } else {
                switch (str) {
                    case "*": {
                        stack.push(stack.pop() * stack.pop());
                        break;
                    }
                    case "/": {
                        tmpNum = stack.pop();
                        if (tmpNum == 0)
                            return null;
                        stack.push(stack.pop() / tmpNum);
                        break;
                    }
                    case "+": {
                        stack.push(stack.pop() + stack.pop());
                        break;
                    }
                    case "-": {
                        tmpNum = stack.pop();
                        stack.push(stack.pop() - tmpNum);
                        break;
                    }
                }
            }
        }
        //rounding output
        int resInt;
        if (!hasDot) {
            int res = stack.pop().intValue();
            return Integer.toString(res);
        }
        else{
            return stack.pop().toString();
        }
    }
}